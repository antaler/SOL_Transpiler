package main

import (
	"fmt"
	"strings"

	parser "github.com/AntonioAlejandro01/SOL_Parser/src"
)

type Transpiler struct {
	program parser.Program
}

func NewTranspiler(programString string) *Transpiler {
	parser := parser.NewParser(programString)

	program := parser.ParseProgram()

	return &Transpiler{program: program}
}

func (t *Transpiler) Transpile() []byte {
	var sb strings.Builder

	// set package main to can be build
	sb.WriteString("package main\n")
	// imports
	sb.WriteString("import (\"log\";\"net/http\";\"time\";\"fmt\")\n")
	// types
	sb.WriteString("type router struct {rules map[string]map[string]http.HandlerFunc}\ntype server struct {port string;router *router}\ntype middleware func(http.HandlerFunc) http.HandlerFunc\n")
	// News
	sb.WriteString("func NewServer(port string) *server { return &server{port:port,router: NewRouter()}}\nfunc NewRouter() *router {return &router{rules: make(map[string]map[string]http.HandlerFunc)}}\n")
	// funcs
	sb.WriteString("func (s *server) Handle(method string, path string, handler http.HandlerFunc) {_, exist := s.router.rules[path];if !exist {s.router.rules[path] = make(map[string]http.HandlerFunc)};s.router.rules[path][method] = handler}\nfunc (s *server) AddMiddleware(f http.HandlerFunc, middlewares ...middleware) http.HandlerFunc {for _, mid := range middlewares {f = mid(f)};return f}\nfunc (s *server) Listen() error {http.Handle(\"/\", s.router);err := http.ListenAndServe(s.port, nil);return err}\nfunc (r *router) FindHandler(path string, method string) (http.HandlerFunc, bool, bool) {_, exist := r.rules[path];handler, methodExists := r.rules[path][method];return handler, methodExists, exist}\nfunc (r *router) ServeHTTP(w http.ResponseWriter, request *http.Request) {handler, methodExists, exist := r.FindHandler(request.URL.Path, request.Method);if !exist {w.WriteHeader(http.StatusNotFound);return};if !methodExists {w.WriteHeader(http.StatusMethodNotAllowed);return};handler(w, request)}\nfunc logging() middleware {return func(f http.HandlerFunc) http.HandlerFunc {return func(w http.ResponseWriter, request *http.Request) {;start := time.Now();defer func() {log.Println(request.URL.Path, time.Since(start))}();f(w, request)}}}\n")

	serviceStatement := parser.ServiceStatement(t.program)
	endpoints := []*endpoint{}
	for _, base := range serviceStatement.Bases {
		for _, ep := range base.Endpoints {
			urlendpoint := fmt.Sprintf("/%s/%s", base.BasePath, ep.Endpoint)
			for key, method := range ep.Methods {
				endpoints = append(endpoints, &endpoint{
					end:     urlendpoint,
					handler: method.Handler,
					method:  key,
				})
			}
		}
	}
	routeTemplate := "server.Handle(\"%s\", \"%s\", server.AddMiddleware(%s, logging()));"
	var endpointSb strings.Builder
	for _, endpoint := range endpoints {
		endpointSb.WriteString(fmt.Sprintf(routeTemplate, endpoint.method, endpoint.end, endpoint.handler))
	}
	sb.WriteString(fmt.Sprintf("func main() {server := NewServer(\":8080\");%s err := server.Listen();if err != nil {fmt.Println(\"ERROR:>\", err)}}", endpointSb.String()))

	return []byte(sb.String())
}

type endpoint struct {
	end     string
	handler string
	method  string
	mid     string
}

// func main() {

// 	err := ioutil.WriteFile("server/server.go", NewTranspiler("service : {base random:{text: { GET as handlerText : {body : {} params: {size as int,} headers: {Authorization as token,Cookie as cookie,}}}} options:{} before:{* as middlewareAuth,text as middlewareAuthText,} errorsHandlers : {Error as  handlerError, CustomError as handleError,}}").Transpile(), 0644)

// 	if err != nil {
// 		panic(err)
// 	}
// }
